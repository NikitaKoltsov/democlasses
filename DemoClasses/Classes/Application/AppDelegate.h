//
//  AppDelegate.h
//  DemoClasses
//
//  Created by Nikita Koltsov on 4/10/18.
//  Copyright © 2018 n.koltsov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


//
//  HomeDataSource.h
//  mix
//
//  Created by Nikita Koltsov on 3/27/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeScreenTabsHelper.h"

@class HomeDataSource;

@protocol HomeDataSourceDelegate

- (void)homeDataSourceChanged:(HomeDataSource *)homeDataSource;

@end

@interface HomeDataSource : NSObject

@property (weak, nonatomic) id<HomeDataSourceDelegate> delegate;

- (HomeViewControllerSectionType)sectionTypeForIndexPath:(NSIndexPath *)indexPath;
- (NSUInteger)numberOfObjectsInSection:(NSInteger)section;
- (id)objectAtIndexPath:(NSIndexPath *)indexPath;

- (void)loadDataSource;
- (void)updateDataSource;

@end

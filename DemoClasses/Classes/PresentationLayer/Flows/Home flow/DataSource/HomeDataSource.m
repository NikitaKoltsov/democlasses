//
//  HomeDataSource.m
//  mix
//
//  Created by Nikita Koltsov on 3/27/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import "HomeDataSource.h"

#import "FeaturedMixesListModel.h"
#import "DiscoverListModel.h"
#import "TopTagsListModel.h"

#import "TagMixesCellViewModel.h"
#import "MostPopularCellViewModel.h"
#import "GenresCellViewModel.h"
#import "PopularArtistsCellViewModel.h"

#import "MixesWithCommonTagModel.h"

#import "MXConstants.h"

@interface HomeDataSource () <FeaturedMixesListModelDelegate, DiscoverListModelDelegate, TopTagsListModelDelegate>

@property (strong, nonatomic) NSMutableArray *tabsTypesArray;
@property (strong, nonatomic) NSMutableArray *dataSourceArray;

@property (strong, nonatomic) FeaturedMixesListModel *featuredListModel;
@property (strong, nonatomic) DiscoverListModel *discoverListModel;
@property (strong, nonatomic) TopTagsListModel *topTagsListModel;

@end

@implementation HomeDataSource

#pragma mark - Getters/setters

- (FeaturedMixesListModel *)featuredListModel {
    if (!_featuredListModel) {
        _featuredListModel = [FeaturedMixesListModel new];
        _featuredListModel.delegate = self;
    }
    
    return _featuredListModel;
}

- (DiscoverListModel *)discoverListModel {
    if (!_discoverListModel) {
        _discoverListModel = [DiscoverListModel new];
        _discoverListModel.delegate = self;
    }
    
    return _discoverListModel;
}

- (TopTagsListModel *)topTagsListModel {
    if (!_topTagsListModel) {
        _topTagsListModel = [TopTagsListModel new];
        _topTagsListModel.delegate = self;
    }
    
    return _topTagsListModel;
}

- (NSMutableArray *)dataSourceArray {
    if (!_dataSourceArray) {
        _dataSourceArray = [NSMutableArray arrayWithObjects:[GenresCellViewModel defaultViewModel], nil];
    }
    
    return _dataSourceArray;
}

#pragma mark - Methods

- (HomeViewControllerSectionType)sectionTypeForIndexPath:(NSIndexPath *)indexPath {
    id viewModel = self.dataSourceArray[indexPath.row];
    return [HomeScreenTabsHelper sectionTypeForViewModel:viewModel];
}

- (NSUInteger)numberOfObjectsInSection:(NSInteger)section {
    return self.dataSourceArray.count;
}

- (id)objectAtIndexPath:(NSIndexPath *)indexPath {
    return self.dataSourceArray[indexPath.row];
}

- (void)loadDataSource {
    [self clearDataSourceArray];
    
    [self.featuredListModel loadFeaturedMixes];
    
    [self.discoverListModel updateDiscoverUserSuggestions];
    
    [self.topTagsListModel loadTopTags];
}

- (void)updateDataSource {
    [self.featuredListModel updateFeaturedMixes];
}

- (void)manualReloadDataSource {
    [self clearDataSourceArray];
    
    [self.featuredListModel downloadFeaturedMixes];
    
    [self.discoverListModel updateDiscoverUserSuggestions];
    
    [self.topTagsListModel loadTopTags];
}

#pragma mark - FeaturedArtistListModelDelegate

- (void)featuredMixesListModel:(FeaturedMixesListModel *)featuredMixesListModel
                       newData:(NSArray *)mixesList {
    MostPopularCellViewModel *newViewModel = [[MostPopularCellViewModel alloc] initWithMixesList:mixesList
                                                                                           title:kFeaturedMixes
                                                                                        subtitle:kFeaturedMixesSubtitle];
    if (newViewModel.mixesList.count > 0) {
        [self addMostPopularViewModel:newViewModel];
        [self.delegate homeDataSourceChanged:self];
    }
}

#pragma mark - DiscoverListModelDelegate

- (void)discoverListModelChanged:(DiscoverListModel *)discoverListModel {
    PopularArtistsCellViewModel *newViewModel = [[PopularArtistsCellViewModel alloc] initWithUserModelList:discoverListModel.userArray];
    [self addPopularArtistsViewModel:newViewModel];
    
    [self.delegate homeDataSourceChanged:self];
}

#pragma mark - TopTagsListModelDelegate


- (void)topTagsListModel:(TopTagsListModel *)topTagsListModel
                 newData:(NSArray <MixesWithCommonTagModel *> *)newData {
    for (MixesWithCommonTagModel *mixesWithTag in newData) {
        TagMixesCellViewModel *newViewModel = [TagMixesCellViewModel initFromMixesWithCommonTagModel:mixesWithTag];
        
        [self addTagMixesViewModel:newViewModel];
        
    }
    
    [self.delegate homeDataSourceChanged:self];
}

- (void)topTagsListModel:(TopTagsListModel *)topTagsListModel
           updatingError:(NSError *)error {
    
}


#pragma mark - Adding view models

- (void)addMostPopularViewModel:(MostPopularCellViewModel *)newViewModel {
    id viewModel = self.dataSourceArray[0];
    if ([viewModel isKindOfClass:[MostPopularCellViewModel class]]) {
        [self.dataSourceArray replaceObjectAtIndex:0 withObject:newViewModel];
    } else {
        [self.dataSourceArray insertObject:newViewModel atIndex:0];
    }
}

- (void)addPopularArtistsViewModel:(PopularArtistsCellViewModel *)viewModel {
    NSInteger indexOfGenres = [self indexOfGenresViewModel];
    NSInteger tagViewModelCountAfterGenresTab = 0;
    for (NSInteger i = indexOfGenres + 1; i < self.dataSourceArray.count; i++) {
        tagViewModelCountAfterGenresTab++;
    }
    if (tagViewModelCountAfterGenresTab < 2) {
        [self.dataSourceArray addObject:viewModel];
    } else {
        [self.dataSourceArray insertObject:viewModel atIndex:indexOfGenres + 2];
    }
}

- (void)addTagMixesViewModel:(TagMixesCellViewModel *)newViewModel {
    NSInteger tagViewModelsCount = [self tagMixesViewModelsCount];
    if (tagViewModelsCount >= 2) {
        [self.dataSourceArray addObject:newViewModel];
    } else if (tagViewModelsCount == 0) {
        NSInteger indexOfGenres = [self indexOfGenresViewModel];
        if (indexOfGenres == 0) {
            [self.dataSourceArray insertObject:newViewModel atIndex:0];
        } else {
            [self.dataSourceArray insertObject:newViewModel atIndex:indexOfGenres];
        }
    } else if (tagViewModelsCount == 1) {
        NSInteger indexOfGenres = [self indexOfGenresViewModel];
        if (indexOfGenres == self.dataSourceArray.count - 1) {
            [self.dataSourceArray addObject:newViewModel];
        } else {
            [self.dataSourceArray insertObject:newViewModel atIndex:indexOfGenres + 1];
        }
    }
}

#pragma mark - Convenience

- (NSInteger)tagMixesViewModelsCount {
    NSInteger result = 0;
    for (id viewModel in self.dataSourceArray) {
        if ([viewModel isKindOfClass:[TagMixesCellViewModel class]]) {
            result++;
        }
    }
    
    return result;
}

- (NSInteger)indexOfGenresViewModel {
    NSInteger indexOfGenres = 0;
    for (NSInteger i = 0; i < self.dataSourceArray.count; i++) {
        id viewModel = self.dataSourceArray[i];
        if ([HomeScreenTabsHelper sectionTypeForViewModel:viewModel] == HomeViewControllerSectionTypeGenres) {
            indexOfGenres = i;
            break;
        }
    }
    
    return indexOfGenres;
}

- (void)clearDataSourceArray {
    [self.dataSourceArray removeAllObjects];
    [self.dataSourceArray addObject:[GenresCellViewModel defaultViewModel]];
}

@end

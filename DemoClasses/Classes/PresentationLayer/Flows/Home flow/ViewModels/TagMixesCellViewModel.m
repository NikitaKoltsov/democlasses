//
//  TagMixesCellViewModel.m
//  mix
//
//  Created by Nikita Koltsov on 3/27/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import "TagMixesCellViewModel.h"
#import "MXConstants.h"
#import "MixesWithCommonTagModel.h"
#import "TagsDataTransformer.h"

@implementation TagMixesCellViewModel

#pragma mark - Getters/setters

- (NSString *)title {
    return [TagsDataTransformer tagNameWithSharp:self.tagName];
}

#pragma mark - Lifecycle

- (instancetype)initWithMixesList:(NSArray <MXMix *> *)mixesList
                          tagName:(NSString *)tagName
                         subtitle:(NSString *)subtitle {
    self = [super init];
    
    if (self) {
        _tagName = tagName;
        _mixesList = mixesList;
        _subtitle = subtitle;
    }
    
    return self;
}

+ (instancetype)initFromMixesWithCommonTagModel:(MixesWithCommonTagModel *)mixesWithCommonTagModel {
    TagMixesCellViewModel *vm = [TagMixesCellViewModel new];
    vm.tagName = mixesWithCommonTagModel.tagName;
    vm.mixesList = mixesWithCommonTagModel.mixesList;
    vm.subtitle = kRecommendedForYou;
    
    return vm;
}

@end

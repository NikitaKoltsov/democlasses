//
//  TagMixesCellViewModel.h
//  mix
//
//  Created by Nikita Koltsov on 3/27/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MXMix+CoreDataProperties.h"

@class MixesWithCommonTagModel;

@interface TagMixesCellViewModel : NSObject

@property (strong, nonatomic) NSArray <MXMix *> *mixesList;
@property (strong, nonatomic) NSString *tagName;
@property (strong, nonatomic) NSString *subtitle;
@property (nonatomic, readonly) NSString *title;

- (instancetype)initWithMixesList:(NSArray <MXMix *> *)mixesList
                          tagName:(NSString *)tagName
                         subtitle:(NSString *)subtitle;

+ (instancetype)initFromMixesWithCommonTagModel:(MixesWithCommonTagModel *)mixesWithCommonTagModel;

@end

//
//  GenresViewModel.h
//  mix
//
//  Created by Nikita Koltsov on 3/27/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import "GenreModel.h"

@interface GenresCellViewModel : NSObject

@property (strong, nonatomic) NSArray <GenreModel *> *genresList;

- (instancetype)initWithGenresList:(NSArray <GenreModel *> *)genreslist;
+ (instancetype)defaultViewModel;

@end

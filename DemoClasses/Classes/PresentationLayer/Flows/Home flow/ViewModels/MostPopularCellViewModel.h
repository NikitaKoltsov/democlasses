//
//  MostPopularCellViewModel.h
//  mix
//
//  Created by Nikita Koltsov on 3/27/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MXMix+CoreDataProperties.h"

@interface MostPopularCellViewModel : NSObject

@property (strong, nonatomic) NSArray <MXMix *> *mixesList;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *subtitle;

- (instancetype)initWithMixesList:(NSArray <MXMix *> *)mixesList
                            title:(NSString *)title
                         subtitle:(NSString *)subtitle;

+ (instancetype)placeholderViewModel;

@end

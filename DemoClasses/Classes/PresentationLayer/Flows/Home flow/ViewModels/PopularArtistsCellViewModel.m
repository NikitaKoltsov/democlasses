//
//  PopularArtistsViewModel.m
//  mix
//
//  Created by Nikita Koltsov on 3/27/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import "PopularArtistsCellViewModel.h"

@implementation PopularArtistsCellViewModel

- (instancetype)initWithUserModelList:(NSArray <UserModel *> *)userModelList {
    self = [super init];
    
    if (self) {
        _userModelList = userModelList;
    }
 
    return self;
}

@end

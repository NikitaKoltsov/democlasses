//
//  PopularArtistsViewModel.h
//  mix
//
//  Created by Nikita Koltsov on 3/27/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserModel.h"

@interface PopularArtistsCellViewModel : NSObject

@property (strong, nonatomic) NSArray <UserModel *> *userModelList;

- (instancetype)initWithUserModelList:(NSArray <UserModel *> *)userModelList;

@end

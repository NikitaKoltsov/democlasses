//
//  MostPopularCellViewModel.m
//  mix
//
//  Created by Nikita Koltsov on 3/27/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import "MostPopularCellViewModel.h"
#import "MXConstants.h"

@implementation MostPopularCellViewModel

#pragma mark - Lifecycle

- (instancetype)initWithMixesList:(NSArray <MXMix *> *)mixesList
                            title:(NSString *)title
                         subtitle:(NSString *)subtitle {
    self = [super init];
    
    if (self) {
        _mixesList = mixesList;
        _title = title;
        _subtitle = subtitle;
    }
    
    return self;
}

+ (instancetype)placeholderViewModel {
    return [[MostPopularCellViewModel alloc] initWithMixesList:@[]
                                                         title:kFeaturedMixes
                                                      subtitle:@""];
}

@end

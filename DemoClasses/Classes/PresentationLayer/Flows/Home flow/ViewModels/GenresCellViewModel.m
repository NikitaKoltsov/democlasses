//
//  GenresViewModel.m
//  mix
//
//  Created by Nikita Koltsov on 3/27/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import "GenresCellViewModel.h"

@implementation GenresCellViewModel

- (instancetype)initWithGenresList:(NSArray<GenreModel *> *)genreslist
{
    self = [super init];
    
    if (self) {
        _genresList = genreslist;
    }
    return self;
}

+ (instancetype)defaultViewModel {
    NSArray *defaultGenres = @[ [[GenreModel alloc] initWithName:@"Rock" image:[UIImage imageNamed:@"rock"]],
                                [[GenreModel alloc] initWithName:@"Alternative" image:[UIImage imageNamed:@"alternative"]],
                                [[GenreModel alloc] initWithName:@"Grunge" image:[UIImage imageNamed:@"grunge"]],
                                [[GenreModel alloc] initWithName:@"Jazz" image:[UIImage imageNamed:@"jazz"]] ];
    return [[GenresCellViewModel alloc] initWithGenresList:defaultGenres];
}

@end

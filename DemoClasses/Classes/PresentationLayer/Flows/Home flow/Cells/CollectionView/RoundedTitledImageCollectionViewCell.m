//
//  RoundedTitledImageCollectionViewCell.m
//  mix
//
//  Created by Nikita Koltsov on 3/28/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import "RoundedTitledImageCollectionViewCell.h"

@interface RoundedTitledImageCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIView *imageContainerView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation RoundedTitledImageCollectionViewCell

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self updateCorners];
}

#pragma mark - View setup

- (void)populateWithGenreModel:(GenreModel *)genreModel {
    self.titleLabel.text = genreModel.name;
    if (!genreModel.image) {
        self.imageView.backgroundColor = [UIColor lightGrayColor];
    } 
    self.imageView.image = genreModel.image;
}

- (void)updateCorners {
//    self.imageView.layer.cornerRadius = 10.0f;
//    self.imageView.layer.masksToBounds = YES;
}
@end

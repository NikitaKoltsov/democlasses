//
//  PopularArtistCollectionViewCell.m
//  mix
//
//  Created by Nikita Koltsov on 3/28/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import "PopularArtistCollectionViewCell.h"
#import <UIImageView+WebCache.h>

#import "UserModel.h"

#import "ColorScheme.h"
#import "MXConstants.h"

@interface PopularArtistCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *artistImageView;
@property (weak, nonatomic) IBOutlet UILabel *artistNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *followButton;

@end

@implementation PopularArtistCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.artistImageView.layer.masksToBounds = YES;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self updateCorners];
}

- (void)layoutIfNeeded {
    [super layoutIfNeeded];
    
    [self updateCorners];
}

#pragma mark - View setup

- (void)updateCorners {
    self.artistImageView.layer.cornerRadius = CGRectGetHeight(self.artistImageView.bounds) / 2;
}

- (void)populateWithUserModel:(UserModel *)user {
    self.artistImageView.image = nil;
    [self.artistImageView sd_cancelCurrentImageLoad];
    [self.artistImageView sd_setImageWithURL:[NSURL URLWithString:user.avatarUrl]
                            placeholderImage:[UIImage imageNamed:kDefaultAvatarImageName]];
    
    self.artistNameLabel.text = user.userName;
    
    [self reloadButtonStateWithFollow:user.iFollow];
    
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

- (void)reloadButtonStateWithFollow:(BOOL)iFollow {
    if (iFollow) {
        [self.followButton setTitle:@"Following" forState:UIControlStateNormal];
        [self.followButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        self.followButton.userInteractionEnabled = NO;
    } else {
        [self.followButton setTitle:@"Follow" forState:UIControlStateNormal];
        [self.followButton setTitleColor:[UIColor customBlueColor] forState:UIControlStateNormal];
        self.followButton.userInteractionEnabled = YES;
    }
}

#pragma mark - Actions

- (IBAction)followAction:(UIButton *)sender {
    __weak __typeof(self) weakSelf = self;
    
    [self.delegate popularArtistCollectionViewCell:self followActionWithCompletion:^(BOOL success) {
        if (success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf reloadButtonStateWithFollow:YES];
            });
        }
    }];
    
//    [self reloadButtonStateWithFollow:YES];
}

@end

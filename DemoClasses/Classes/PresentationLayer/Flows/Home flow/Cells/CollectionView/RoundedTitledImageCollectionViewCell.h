//
//  RoundedTitledImageCollectionViewCell.h
//  mix
//
//  Created by Nikita Koltsov on 3/28/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GenreModel.h"

@interface RoundedTitledImageCollectionViewCell : UICollectionViewCell

- (void)populateWithGenreModel:(GenreModel *)genreModel;

@end

//
//  PopularArtistCollectionViewCell.h
//  mix
//
//  Created by Nikita Koltsov on 3/28/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UserModel, PopularArtistCollectionViewCell;

@protocol PopularArtistCollectionViewCellDelegate

- (void)popularArtistCollectionViewCell:(PopularArtistCollectionViewCell *)popularArtistCollectionViewCell
             followActionWithCompletion:(void (^)(BOOL))completion;

@end

@interface PopularArtistCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) id<PopularArtistCollectionViewCellDelegate> delegate;

- (void)populateWithUserModel:(UserModel *)user;

@end

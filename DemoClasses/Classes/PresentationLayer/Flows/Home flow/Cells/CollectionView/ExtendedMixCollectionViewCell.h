//
//  ExtendedMixCollectionViewCell.h
//  mix
//
//  Created by Nikita Koltsov on 3/27/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ExtendedMixCollectionViewCellType) {
    ExtendedMixCollectionViewCellTypeWithLikesCommentsInfo,
    ExtendedMixCollectionViewCellTypePlain
};

@class MXMix;

@interface ExtendedMixCollectionViewCell : UICollectionViewCell

- (void)populateWithMix:(MXMix *)mix;

@property (assign, nonatomic) ExtendedMixCollectionViewCellType type;

@end

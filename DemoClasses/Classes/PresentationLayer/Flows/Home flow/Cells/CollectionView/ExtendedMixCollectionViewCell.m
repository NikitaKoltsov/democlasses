//
//  ExtendedMixCollectionViewCell.m
//  mix
//
//  Created by Nikita Koltsov on 3/27/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import "ExtendedMixCollectionViewCell.h"
#import <UIImageView+WebCache.h>

#import "MXMix+MixFunctions.h"

@interface ExtendedMixCollectionViewCell()

// Images
@property (weak, nonatomic) IBOutlet UIImageView *mixImageView;
@property (weak, nonatomic) IBOutlet UIImageView *playIconImageView;
// Text
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
// Likes/comments
@property (weak, nonatomic) IBOutlet UIView *likesCommentsContainerView;
@property (weak, nonatomic) IBOutlet UILabel *likeCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentsCountLabel;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UIButton *commentsButton;

@end

@implementation ExtendedMixCollectionViewCell

#pragma mark - Lifecycle

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.mixImageView.layer.masksToBounds = YES;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self updateCorners];
}

#pragma mark - Setup
- (void)populateWithMix:(MXMix *)mix {
    switch (self.type) {
        case ExtendedMixCollectionViewCellTypePlain:
            self.likesCommentsContainerView.hidden = YES;
            self.likeCountLabel.text = @"";
            self.commentsCountLabel.text = @"";
            break;
        case ExtendedMixCollectionViewCellTypeWithLikesCommentsInfo:
            self.likesCommentsContainerView.hidden = NO;
            self.likeCountLabel.text = mix.likeCount.stringValue;
            self.commentsCountLabel.text = mix.commentCount.stringValue;
            break;
    }
    
    self.mixImageView.image = nil;
    [self.mixImageView sd_cancelCurrentImageLoad];
    [self.mixImageView sd_setImageWithURL:[NSURL URLWithString:mix.imageURL]];
    
    UIImage *playIconImage = mix.isPublic.boolValue ? [UIImage imageNamed:@"playIconLarge"] : [UIImage imageNamed:@"lockIconLarge"];
    self.playIconImageView.image = playIconImage;
    
    self.titleLabel.text = mix.name;
    self.subtitleLabel.text = mix.artistName;
}

#pragma mark - View customization

- (void)updateCorners {
    self.mixImageView.layer.cornerRadius = 10.0f;
}


@end

//
//  GenresTableViewCell.h
//  mix
//
//  Created by Nikita Koltsov on 3/26/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GenresCellViewModel.h"

@class GenresTableViewCell;

@protocol GenresTableViewCellDelegate

- (void)genresTableViewCellView:(GenresTableViewCell *)genresTableViewCell
                 didSelectModel:(GenreModel *)viewModel;

@end

@interface GenresTableViewCell : UITableViewCell

@property (weak, nonatomic) id<GenresTableViewCellDelegate> delegate;

- (void)populateWithViewModel:(GenresCellViewModel *)viewModel;

@end

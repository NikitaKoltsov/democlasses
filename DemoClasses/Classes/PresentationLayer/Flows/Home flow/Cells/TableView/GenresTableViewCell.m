//
//  GenresTableViewCell.m
//  mix
//
//  Created by Nikita Koltsov on 3/26/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import "GenresTableViewCell.h"

#import "RoundedTitledImageCollectionViewCell.h"

@interface GenresTableViewCell () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) GenresCellViewModel *viewModel;

@end

@implementation GenresTableViewCell

#pragma mark - Lifecycle

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self setupCollectionView];
}

#pragma mark - View setup

- (void)setupCollectionView {
    NSString *cellIdentifier = NSStringFromClass([RoundedTitledImageCollectionViewCell class]);
    [self.collectionView registerNib:[UINib nibWithNibName:cellIdentifier bundle:nil]
          forCellWithReuseIdentifier:cellIdentifier];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
}

- (void)populateWithViewModel:(GenresCellViewModel *)viewModel {
    self.viewModel = viewModel;
    [self.collectionView reloadData];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    return self.viewModel.genresList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    RoundedTitledImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RoundedTitledImageCollectionViewCell class])
                                                                                    forIndexPath:indexPath];
    [cell populateWithGenreModel:self.viewModel.genresList[indexPath.row]];
    
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat cellHeight = CGRectGetHeight(collectionView.frame) * 0.95f;
    CGFloat cellWidth = cellHeight;
    CGSize cellSize = CGSizeMake(cellWidth, cellHeight);
    return cellSize;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    GenreModel *selectedModel = self.viewModel.genresList[indexPath.row];
    [self.delegate genresTableViewCellView:self didSelectModel:selectedModel];
}

//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView
//                        layout:(UICollectionViewLayout *)collectionViewLayout
//        insetForSectionAtIndex:(NSInteger)section {
//    return UIEdgeInsetsMake(0.0f, 9.0f, 0.0f, 0.0f);
//}


@end

//
//  PopularArtistsTableViewCell.m
//  mix
//
//  Created by Nikita Koltsov on 3/26/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import "PopularArtistsTableViewCell.h"
#import "PopularArtistCollectionViewCell.h"
#import "MXConstants.h"

@interface PopularArtistsTableViewCell () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, PopularArtistCollectionViewCellDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) PopularArtistsCellViewModel *viewModel;

@end

@implementation PopularArtistsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self setupCollectionView];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleNotification:)
                                                 name:kNotificationUserFollowed
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleNotification:)
                                                 name:kNotificationUserUnfollowed
                                               object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)handleNotification:(NSNotification *)notification {
    if ([notification.name isEqualToString:kNotificationUserUnfollowed] || [notification.name isEqualToString:kNotificationUserFollowed]) {
        NSString *userID = notification.userInfo[kNotificationDataUserId];
        NSMutableArray <NSIndexPath *> *indexPaths = [NSMutableArray array];
        
        for (NSInteger index = 0; index < self.viewModel.userModelList.count; index++) {
            UserModel *userModel = self.viewModel.userModelList[index];
            if ([userModel.userId isEqualToString:userID]) {
                userModel.iFollow = [notification.name isEqualToString:kNotificationUserFollowed];
                [indexPaths addObject:[NSIndexPath indexPathForRow:index inSection:0]];
            }
        }
        
        if (indexPaths.count > 0) {
            [self.collectionView reloadItemsAtIndexPaths:indexPaths];
        }
    }
}

#pragma mark - Setup view

- (void)setupCollectionView {
    NSString *cellIdentifier = NSStringFromClass([PopularArtistCollectionViewCell class]);
    [self.collectionView registerNib:[UINib nibWithNibName:cellIdentifier bundle:nil]
          forCellWithReuseIdentifier:cellIdentifier];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
}

- (void)populateWithViewModel:(PopularArtistsCellViewModel *)viewModel {
    self.viewModel = viewModel;
    [self.collectionView reloadData];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    return self.viewModel.userModelList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PopularArtistCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([PopularArtistCollectionViewCell class])
                                                                                           forIndexPath:indexPath];
    [cell populateWithUserModel:self.viewModel.userModelList[indexPath.row]];
    cell.delegate = self;
    
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat cellHeight = CGRectGetHeight(collectionView.frame) * 0.95f;
    CGFloat cellWidth = cellHeight * 0.8;
    CGSize cellSize = CGSizeMake(cellWidth, cellHeight);
    return cellSize;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    UserModel *selectedModel = self.viewModel.userModelList[indexPath.row];
    [self.delegate popularArtistsTableViewCell:self didSelectUserModel:selectedModel];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 10.0f;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView
                        layout:(UICollectionViewLayout *)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
}

#pragma mark - PopularArtistCollectionViewCellDelegate

- (void)popularArtistCollectionViewCell:(PopularArtistCollectionViewCell *)popularArtistCollectionViewCell
             followActionWithCompletion:(void (^)(BOOL))completion {
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:popularArtistCollectionViewCell];
    UserModel *selectedUserModel = self.viewModel.userModelList[indexPath.row];
    [self.delegate popularArtistsTableViewCell:self followUser:selectedUserModel];
}

@end

//
//  MostPopularTableViewCell.m
//  mix
//
//  Created by Nikita Koltsov on 3/26/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import "MostPopularTableViewCell.h"

#import "ExtendedMixCollectionViewCell.h"

@interface MostPopularTableViewCell() <UICollectionViewDelegateFlowLayout, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) MostPopularCellViewModel *viewModel;

@end

@implementation MostPopularTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self setupCollectionView];
}

#pragma mark - View setup

- (void)setupCollectionView {
    NSString *cellIdentifier = NSStringFromClass([ExtendedMixCollectionViewCell class]);
    [self.collectionView registerNib:[UINib nibWithNibName:cellIdentifier bundle:nil]
          forCellWithReuseIdentifier:cellIdentifier];
    self.collectionView.alwaysBounceHorizontal = YES;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
}

- (void)populateWithViewModel:(MostPopularCellViewModel *)viewModel {
    self.viewModel = viewModel;
    self.titleLabel.text = viewModel.title;
    self.subtitleLabel.text = viewModel.subtitle;
    [self.collectionView reloadData];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    return self.viewModel.mixesList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ExtendedMixCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([ExtendedMixCollectionViewCell class])
                                                                                    forIndexPath:indexPath];
    
    [cell populateWithMix:self.viewModel.mixesList[indexPath.row]];
    
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat cellHeight = CGRectGetHeight(collectionView.frame) * 0.95f;
    CGFloat cellWidth = cellHeight * 1.1;
    CGSize cellSize = CGSizeMake(cellWidth, cellHeight);
    return cellSize;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    MXMix *selectedMix = self.viewModel.mixesList[indexPath.row];
    [self.delegate mostPopularTableViewCell:self didSelectMix:selectedMix];
}

@end

//
//  MostPopularTableViewCell.h
//  mix
//
//  Created by Nikita Koltsov on 3/26/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MostPopularCellViewModel.h"

@class MostPopularTableViewCell;

@protocol MostPopularTableViewCellDelegate

- (void)mostPopularTableViewCell:(MostPopularTableViewCell *)mostPopularTableViewCell
                    didSelectMix:(MXMix *)mix;

@end

@interface MostPopularTableViewCell : UITableViewCell

- (void)populateWithViewModel:(MostPopularCellViewModel *)viewModel;

@property (weak, nonatomic) id<MostPopularTableViewCellDelegate> delegate;

@end

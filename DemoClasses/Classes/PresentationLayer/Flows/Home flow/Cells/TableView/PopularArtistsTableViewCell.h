//
//  PopularArtistsTableViewCell.h
//  mix
//
//  Created by Nikita Koltsov on 3/26/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopularArtistsCellViewModel.h"

#import "UserModel.h"

@class PopularArtistsTableViewCell;

@protocol PopularArtistsTableViewCellDelegate

- (void)popularArtistsTableViewCell:(PopularArtistsTableViewCell *)popularArtistsTableViewCell
                 didSelectUserModel:(UserModel *)userModel;
- (void)popularArtistsTableViewCell:(PopularArtistsTableViewCell *)popularArtistsTableViewCell
                         followUser:(UserModel *)userModel;

@end

@interface PopularArtistsTableViewCell : UITableViewCell

@property (weak, nonatomic) id<PopularArtistsTableViewCellDelegate> delegate;

- (void)populateWithViewModel:(PopularArtistsCellViewModel *)viewModel;

@end

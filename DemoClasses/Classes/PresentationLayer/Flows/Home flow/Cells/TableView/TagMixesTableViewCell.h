//
//  TagMixesTableViewCell.h
//  mix
//
//  Created by Nikita Koltsov on 3/26/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TagMixesCellViewModel.h"

@class TagMixesTableViewCell;

@protocol TagMixesTableViewCellDelegate

- (void)tagMixesTableViewCell:(TagMixesTableViewCell *)tagMixesTableViewCell
                 didSelectMix:(MXMix *)mix;
- (void)tagMixesTableViewCellViewAllSelected:(TagMixesTableViewCell *)tagMixesTableViewCell;

@end

@interface TagMixesTableViewCell : UITableViewCell

- (void)populateWithViewModel:(TagMixesTableViewCell *)viewModel;

@property (weak, nonatomic) id<TagMixesTableViewCellDelegate> delegate;
@property (strong, nonatomic, readonly) TagMixesCellViewModel *viewModel;

@end

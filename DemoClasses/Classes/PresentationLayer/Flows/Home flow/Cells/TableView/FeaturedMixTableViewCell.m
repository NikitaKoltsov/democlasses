//
//  FeaturedMixTableViewCell.m
//  mix
//
//  Created by Nikita Koltsov on 3/5/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import "FeaturedMixTableViewCell.h"

#import <MagicalRecord/MagicalRecord.h>

#import "VideoPlayerView.h"

#import "ColorScheme.h"
#import "MixConstants.h"

@interface FeaturedMixTableViewCell() <VideoPlayerViewDelegate>

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;
@property (weak, nonatomic) IBOutlet VideoPlayerView *videoPlayerView;

@end

@implementation FeaturedMixTableViewCell

- (UIImage *)mixImage {
    return self.videoPlayerView.mixImage;
}

#pragma mark - Lifecycle

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setBackgroundColor:[UIColor darkestGreyColor]];
    
    self.videoPlayerView.delegate = self;
    self.videoPlayerView.hideCancelButton = YES;
    self.player = self.videoPlayerView.player;
    
}

#pragma mark - Cell methods

- (void)prepareForReuse {
    [super prepareForReuse];
    
    [self.videoPlayerView cancelVideoImageLoading];
    
    [self.player pause];
    [self.player cancelPendingPrerolls];
}

#pragma mark - Methods

- (void)populateWithMix:(MXMix *)mix {
    self.activityIndicatorView.hidden = NO;
    [self.activityIndicatorView startAnimating];
    
    [self.videoPlayerView resetUIState];
    [self.videoPlayerView populateWithMix:mix];
    [self.videoPlayerView playVideo];
}

- (void)playVideo {
    [self.videoPlayerView playVideo];
}

- (void)pauseVideo {
    [self.videoPlayerView pauseVideo];
}

- (void)stopProgressTracking {
    [self.videoPlayerView stopProgressTracking];
}

#pragma mark - VideoPlayerViewDelegate

- (void)quitButtonActionIn:(VideoPlayerView *)videoPlayerView {
    
}

- (void)videoPlayerView:(VideoPlayerView *)videoPlayerView userSelected:(MXUser *)user {
    if (self.delegate && [self.delegate respondsToSelector:@selector(featuredMixTableViewCell:userSelected:)]) {
        [self.delegate featuredMixTableViewCell:self
                                   userSelected:user];
    }
}

- (void)videoPlayerView:(VideoPlayerView *)videoPlayerView followUser:(MXUser *)user withCompletion:(void (^)(BOOL))completion {
    if (self.delegate && [self.delegate respondsToSelector:@selector(featuredMixTableViewCell:followUser:withCompletion:)]) {
        [self.delegate featuredMixTableViewCell:self
                                     followUser:user
                                 withCompletion:completion];
    }
}

- (void)videoPlayerView:(VideoPlayerView *)videoPlayerView unfollowUser:(MXUser *)user withCompletion:(void (^)(BOOL))completion {
    if (self.delegate && [self.delegate respondsToSelector:@selector(featuredMixTableViewCell:unfollowUser:withCompletion:)]) {
        [self.delegate featuredMixTableViewCell:self
                                   unfollowUser:user
                                 withCompletion:completion];
    }
}

- (void)commentsButtonActionIn:(VideoPlayerView *)videoPlayerView {
    if (self.delegate && [self.delegate respondsToSelector:@selector(commentsActionInFeaturedMixTableViewCell:)]) {
        [self.delegate commentsActionInFeaturedMixTableViewCell:self];
    }
}

- (void)likeButtonActionIn:(VideoPlayerView *)videoPlayerView
                      like:(BOOL)like
            withCompletion:(void (^)(BOOL, NSString *))completion {
    if (self.delegate && [self.delegate respondsToSelector:@selector(featuredMixTableViewCell:like:withCompletion:)]) {
        [self.delegate featuredMixTableViewCell:self
                                           like:like
                                 withCompletion:completion];
    }
}

- (void)shareButtonActionIn:(VideoPlayerView *)videoPlayerView {
    [self pauseVideo];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(shareActionInFeaturedMixTableViewCell:)]) {
        [self.delegate shareActionInFeaturedMixTableViewCell:self];
    }
}

- (void)collaborateButtonActionIn:(VideoPlayerView *)videoPlayerView {
    [self pauseVideo];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(collaborateActionInFeaturedMixTableViewCell:)]) {
        [self.delegate collaborateActionInFeaturedMixTableViewCell:self];
    }
}

- (void)editButtonActionIn:(VideoPlayerView *)videoPlayerView {
    if (self.delegate && [self.delegate respondsToSelector:@selector(editActionInFeaturedMixTableViewCell:)]) {
        [self.delegate editActionInFeaturedMixTableViewCell:self];
    }
}

- (void)videoPlayerView:(VideoPlayerView *)videoPlayerView hashtagTapped:(NSString *)hashtag {
    if (self.delegate && [self.delegate respondsToSelector:@selector(featuredMixTableViewCell:hashTagSelected:)]) {
        [self.delegate featuredMixTableViewCell:self hashTagSelected:hashtag];
    }
}

- (void)readyToPlayInVideoPlayerView:(VideoPlayerView *)videoPlayerView {
    [self.activityIndicatorView stopAnimating];
    self.activityIndicatorView.hidden = YES;
}

@end

//
//  FeaturedMixTableViewCell.h
//  mix
//
//  Created by Nikita Koltsov on 3/5/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SCPlayer.h>

@class FeaturedMixTableViewCell, MXMix, MXUser;

@protocol FeaturedMixTableViewCellDelegate <NSObject>

@optional
- (void)featuredMixTableViewCell:(FeaturedMixTableViewCell *)featuredMixTableViewCell
                      followUser:(MXUser *)user
                  withCompletion:(void (^)(BOOL))completion;
- (void)featuredMixTableViewCell:(FeaturedMixTableViewCell *)featuredMixTableViewCell
                    unfollowUser:(MXUser *)user
                  withCompletion:(void (^)(BOOL))completion;

- (void)featuredMixTableViewCell:(FeaturedMixTableViewCell *)featuredMixTableViewCell
                    userSelected:(MXUser *)user;
- (void)commentsActionInFeaturedMixTableViewCell:(FeaturedMixTableViewCell *)featuredMixTableViewCell;
- (void)featuredMixTableViewCell:(FeaturedMixTableViewCell *)featuredMixTableViewCell
                            like:(BOOL)like
                  withCompletion:(void (^)(BOOL, NSString *))completion;
- (void)shareActionInFeaturedMixTableViewCell:(FeaturedMixTableViewCell *)featuredMixTableViewCell;
- (void)collaborateActionInFeaturedMixTableViewCell:(FeaturedMixTableViewCell *)featuredMixTableViewCell;
- (void)editActionInFeaturedMixTableViewCell:(FeaturedMixTableViewCell *)featuredMixTableViewCell;
- (void)featuredMixTableViewCell:(FeaturedMixTableViewCell *)featuredMixTableViewCell
                    hashTagSelected:(NSString *)hashTag;

@end


@interface FeaturedMixTableViewCell : UITableViewCell

@property (strong, nonatomic) SCPlayer *player;
@property (weak, nonatomic) id<FeaturedMixTableViewCellDelegate> delegate;
@property (nonatomic, readonly) UIImage *mixImage;

- (void)populateWithMix:(MXMix *)mix;
- (void)stopProgressTracking;
- (void)playVideo;
- (void)pauseVideo;
@end

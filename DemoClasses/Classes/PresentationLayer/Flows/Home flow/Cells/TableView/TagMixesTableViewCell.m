//
//  TagMixesTableViewCell.m
//  mix
//
//  Created by Nikita Koltsov on 3/26/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import "TagMixesTableViewCell.h"
#import "ExtendedMixCollectionViewCell.h"

@interface TagMixesTableViewCell () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>


@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;

@property (strong, nonatomic, readwrite) TagMixesCellViewModel *viewModel;

@end

@implementation TagMixesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self setupCollectionView];
}

#pragma mark - View setup

- (void)setupCollectionView {
    NSString *cellIdentifier = NSStringFromClass([ExtendedMixCollectionViewCell class]);
    [self.collectionView registerNib:[UINib nibWithNibName:cellIdentifier bundle:nil]
          forCellWithReuseIdentifier:cellIdentifier];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
}

- (void)populateWithViewModel:(TagMixesCellViewModel *)viewModel {
    self.viewModel = viewModel;
    self.titleLabel.text = viewModel.title;
    self.subtitleLabel.text = viewModel.subtitle;
    [self.collectionView reloadData];
}

#pragma mark - Actions

- (IBAction)viewAllAction:(UIButton *)sender {
    [self.delegate tagMixesTableViewCellViewAllSelected:self];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    return self.viewModel.mixesList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ExtendedMixCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([ExtendedMixCollectionViewCell class])
                                                                                    forIndexPath:indexPath];
    cell.type = ExtendedMixCollectionViewCellTypeWithLikesCommentsInfo;
    [cell populateWithMix:self.viewModel.mixesList[indexPath.row]];
    
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat cellHeight = CGRectGetHeight(collectionView.frame) * 0.95f;
    CGFloat cellWidth = cellHeight * 1.1;
    CGSize cellSize = CGSizeMake(cellWidth, cellHeight);
    return cellSize;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    MXMix *selectedMix = self.viewModel.mixesList[indexPath.row];
    [self.delegate tagMixesTableViewCell:self didSelectMix:selectedMix];
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView
                        layout:(UICollectionViewLayout *)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0.0f, 9.0f, 0.0f, 0.0f);
}

@end

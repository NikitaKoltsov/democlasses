//
//  HomeViewController.m
//  mix
//
//  Created by Nikita Koltsov on 3/26/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import "HomeViewController.h"

#import "ProfileViewController.h"
#import "TrackListViewController.h"
#import "SearchedMixesViewController.h"
#import "MostPopularTableViewCell.h"
#import "TagMixesTableViewCell.h"
#import "GenresTableViewCell.h"
#import "PopularArtistsTableViewCell.h"

#import "MXUser+UserFunctions.h"
#import "MXUserService.h"

#import "MXConstants.h"
#import "ModuleFactory.h"
#import "HomeDataSource.h"

@interface HomeViewController () <UITableViewDataSource, UITableViewDelegate, HomeDataSourceDelegate, MixPlayerViewControllerDelegate, MostPopularTableViewCellDelegate, TagMixesTableViewCellDelegate, GenresTableViewCellDelegate, PopularArtistsTableViewCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UIRefreshControl *refreshControl;

@property (strong, nonatomic) HomeDataSource *dataSource;

@end

@implementation HomeViewController

#pragma mark - Getters/setters

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

- (HomeDataSource *)dataSource {
    if (!_dataSource) {
        _dataSource = [HomeDataSource new];
        _dataSource.delegate = self;
    }
    
    return _dataSource;
}

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupTableView];
    [self.dataSource loadDataSource];
    
    [self activateNotifications];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self updateDataSource];
}

#pragma mark - View customization

- (void)setupTableView {
    NSString *mostPopularCellIdentifier = NSStringFromClass([MostPopularTableViewCell class]);
    [self.tableView registerNib:[UINib nibWithNibName:mostPopularCellIdentifier bundle:nil]
         forCellReuseIdentifier:mostPopularCellIdentifier];
    
    NSString *tagMixesCellIdentifier = NSStringFromClass([GenresTableViewCell class]);
    [self.tableView registerNib:[UINib nibWithNibName:tagMixesCellIdentifier bundle:nil]
         forCellReuseIdentifier:tagMixesCellIdentifier];
    
    NSString *genresCellIdentifier = NSStringFromClass([TagMixesTableViewCell class]);
    [self.tableView registerNib:[UINib nibWithNibName:genresCellIdentifier bundle:nil]
         forCellReuseIdentifier:genresCellIdentifier];
    
    NSString *popularArtistsCellIdentifier = NSStringFromClass([PopularArtistsTableViewCell class]);
    [self.tableView registerNib:[UINib nibWithNibName:popularArtistsCellIdentifier bundle:nil]
         forCellReuseIdentifier:popularArtistsCellIdentifier];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 250.0f;
    self.tableView.tableFooterView = [UIView new];
    
    self.refreshControl = [UIRefreshControl new];
    [self.tableView addSubview:self.refreshControl];
    [self.refreshControl addTarget:self action:@selector(manualRefresh:) forControlEvents:UIControlEventValueChanged];
}

#pragma mark - Setup

- (void)updateDataSource {
    [self.dataSource updateDataSource];
}

- (void)activateNotifications {
    if ([UNUserNotificationCenter class] != nil) {
        UNAuthorizationOptions authOptions = (UNAuthorizationOptionBadge | UNAuthorizationOptionSound | UNAuthorizationOptionAlert);
        [UNUserNotificationCenter.currentNotificationCenter requestAuthorizationWithOptions:authOptions
                                                                          completionHandler:^(BOOL granted, NSError * _Nullable error) {
                                                                              if (granted) {
                                                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                                                      [[UIApplication sharedApplication] registerForRemoteNotifications];
                                                                                  });
                                                                              }
                                                                          }];
    } else {
        UIUserNotificationType allNotificationTypes = (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    }
}

#pragma mark - Methods

- (void)handleUserNotLoggedIn {
    [MXAnalyticsService logEventWithName:@"user_not_logged_in"
                              parameters:@{ kAnalyticsScreenKey : kAnalyticsHomeScreenKey }];
    
    [self showAlertWithTitle:@"Please login" andMessage:@""];
}

#pragma mark - Actions

- (void)manualRefresh:(UIRefreshControl *)sender {
    [MXAnalyticsService logEventWithName:kAnalyticsPullToRefreshEventNameKey
                              parameters:@{ kAnalyticsScreenKey : kAnalyticsHomeScreenKey }];
    
    [self.dataSource loadDataSource];
    [sender endRefreshing];
}

#pragma mark - HomeDataSourceDelegate

- (void)homeDataSourceChanged:(HomeDataSource *)homeDataSource {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dataSource numberOfObjectsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *resultCell = nil;
    switch ([self.dataSource sectionTypeForIndexPath:indexPath]) {
        case HomeViewControllerSectionTypeMostPopular: {
            MostPopularTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([MostPopularTableViewCell class])
                                                                             forIndexPath:indexPath];
            cell.delegate = self;
            [cell populateWithViewModel:[self.dataSource objectAtIndexPath:indexPath]];
            resultCell = cell;
        }
            break;
        case HomeViewControllerSectionTypePopularArtists: {
            PopularArtistsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PopularArtistsTableViewCell class])
                                                                                forIndexPath:indexPath];
            
            cell.delegate = self;
            [cell populateWithViewModel:[self.dataSource objectAtIndexPath:indexPath]];
            
            resultCell = cell;
        }
            break;
        case HomeViewControllerSectionTypeTag: {
            TagMixesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([TagMixesTableViewCell class])
                                                                          forIndexPath:indexPath];
            
            cell.delegate = self;
            [cell populateWithViewModel:[self.dataSource objectAtIndexPath:indexPath]];
            
            resultCell = cell;
        }
            break;
        case HomeViewControllerSectionTypeGenres: {
            GenresTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([GenresTableViewCell class])
                                                                        forIndexPath:indexPath];
            cell.delegate = self;
            [cell populateWithViewModel:[self.dataSource objectAtIndexPath:indexPath]];
            
            resultCell = cell;
        }
            break;
        default:
            break;
    }
    
    return resultCell;
}

#pragma mark - MostPopularTableViewCellDelegate

- (void)mostPopularTableViewCell:(MostPopularTableViewCell *)mostPopularTableViewCell didSelectMix:(MXMix *)mix {
    [MXAnalyticsService logEventWithName:kAnalyticsPlayFeaturedMixEventNameKey
                              parameters:@{ kAnalyticsScreenKey : kAnalyticsHomeScreenKey }];
    
    if (mix) {
        [self presentMixPlayerViewControllerWithMix:mix];
    }
}

#pragma mark - TagMixesTableViewCellDelegate

- (void)tagMixesTableViewCell:(TagMixesTableViewCell *)tagMixesTableViewCell didSelectMix:(MXMix *)mix {
    [MXAnalyticsService logEventWithName:kAnalyticsPlayVideoEventNameKey
                              parameters:@{ kAnalyticsScreenKey : kAnalyticsHomeScreenKey }];
    
    if (mix) {
        [self presentMixPlayerViewControllerWithMix:mix];
    }
}

- (void)tagMixesTableViewCellViewAllSelected:(TagMixesTableViewCell *)tagMixesTableViewCell {
    [MXAnalyticsService logEventWithName:@"tag_mixes_view_all_action"
                              parameters:@{ kAnalyticsScreenKey : kAnalyticsHomeScreenKey }];
    
    TagMixesCellViewModel *selectedViewModel = tagMixesTableViewCell.viewModel;
    if (selectedViewModel) {
        [self showSearchedMixesViewControllerWithHashTag:selectedViewModel.tagName];
    }
}

#pragma mark - GenresTableViewCellDelegate

- (void)genresTableViewCellView:(GenresTableViewCell *)genresTableViewCell
                 didSelectModel:(GenreModel *)model {
    [MXAnalyticsService logEventWithName:@"genre_selected"
                              parameters:@{ kAnalyticsScreenKey : kAnalyticsHomeScreenKey }];
    if (model) {
        [self showSearchedMixesViewControllerWithHashTag:model.name];
    }
}

#pragma mark - PopularArtistsTableViewCellDelegate

- (void)popularArtistsTableViewCell:(PopularArtistsTableViewCell *)popularArtistsTableViewCell
                         followUser:(UserModel *)userModel {
    [MXAnalyticsService logEventWithName:kAnalyticsUserFollowedEventNameKey
                              parameters:@{ kAnalyticsScreenKey : kAnalyticsHomeScreenKey }];
    __weak __typeof(self) weakSelf = self;
    
    [MXUserService followUserWithId:userModel.userId
                   withSuccessBlock:^{
                       
                   } failureBlock:^(NSString *errorString) {
                       [weakSelf showAlertWithTitle:@"Error" andMessage:@"Please try again later."];
                   }];
}

- (void)popularArtistsTableViewCell:(PopularArtistsTableViewCell *)popularArtistsTableViewCell
                 didSelectUserModel:(UserModel *)userModel {
    [MXAnalyticsService logEventWithName:kAnalyticsUserSelectedEventNameKey
                              parameters:@{ kAnalyticsScreenKey : kAnalyticsHomeScreenKey }];
    if (userModel) {
        MXUser *user = [MXUser userFromUserModel:userModel];
        
        if (user) {
            [self showProfileViewControllerForUser:user];
        }
    }
}


#pragma mark - MixPlayerViewControllerDelegate

- (void)closeActionIn:(MixPlayerViewController *)mixPlayerViewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)editActionIn:(MixPlayerViewController *)mixPlayerViewController forMix:(MXMix *)mix {
    if (![MXUser userIsLogedIn]) {
        [self handleUserNotLoggedIn];
        return;
    }
    
    if (mix) {
        MXMix *mixToEdit = [mix duplicateFeaturedMix];
        [self dismissViewControllerAnimated:YES completion:nil];
        [self showTrackListViewControllerForMix:mixToEdit];
    }
}

- (void)mixPlayerViewController:(MixPlayerViewController *)mixPlayerViewController
                   userSelected:(MXUser *)user {
    if (user && [user.userId isEqualToString:[[MXUser masterUser] userId]]) {
        [self dismissViewControllerAnimated:YES completion:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationShowMyProfileScreen object:nil];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
        [self showProfileViewControllerForUser:user];
    }
}

- (void)mixPlayerViewController:(MixPlayerViewController *)mixPlayerViewController
                hashTagSelected:(NSString *)hashTag {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self showSearchedMixesViewControllerWithHashTag:hashTag];
}

- (void)collaborateActionIn:(MixPlayerViewController *)mixPlayerViewController
                     forMix:(MXMix *)mix {
    if (![MXUser userIsLogedIn]) {
        [self handleUserNotLoggedIn];
        return;
    }
    
    if (mix) {
        MXMix *mixToEdit = [mix duplicateFeaturedMix];
        [self dismissViewControllerAnimated:YES completion:nil];
        [self showTrackListViewControllerForMix:mixToEdit];
    }
}

#pragma mark - Navigation

- (void)showProfileViewControllerForUser:(MXUser *)user {
    ProfileViewController *vc = [ModuleFactory profileViewControllerWithUser:user];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)presentMixPlayerViewControllerWithMix:(MXMix *)mix {
    MixPlayerViewController *vc = [ModuleFactory mixPlayerViewControllerWithMix:mix delegate:self];
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)showSearchedMixesViewControllerWithHashTag:(NSString *)hashTag {
    SearchedMixesViewController *vc = [ModuleFactory searchedMixesViewControllerWithSearchTag:hashTag];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showTrackListViewControllerForMix:(MXMix *)mix {
    TrackListViewController *controller = [ModuleFactory trackListViewControllerWithMix:mix featuredEdit:YES];
    
    [self.tabBarController.navigationController pushViewController:controller animated:YES];
}


@end

//
//  NotificationsDataTransformer.h
//  mix
//
//  Created by Nikita Koltsov on 3/19/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MXNotification;

@interface NotificationsDataTransformer : NSObject

+ (NSString *)notificationDescriptionForNotification:(MXNotification *)notification;

@end

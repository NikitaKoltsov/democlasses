//
//  NotificationsDataTransformer.m
//  mix
//
//  Created by Nikita Koltsov on 3/19/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import "NotificationsDataTransformer.h"

#import "MXNotification+NotificationFunctions.h"

@implementation NotificationsDataTransformer

+ (NSString *)notificationDescriptionForNotification:(MXNotification *)notification {
    NSString *notificationStringSentence = @"";
    
    NotificationType notificationType = notification.notificationType.intValue;
    switch (notificationType) {
        case NotificationTypeLike:
            notificationStringSentence = [NSString stringWithFormat:@"liked your mix"];
            break;
        case NotificationTypeComment:
            notificationStringSentence = [NSString stringWithFormat:@"commented on your mix"];
            break;
        case NotificationTypeFollowing:
            notificationStringSentence = [NSString stringWithFormat:@"started following you"];
            break;
        case NotificationTypeColaborated:
            notificationStringSentence = [NSString stringWithFormat:@"collabed with your track"];
            break;
        default:
            break;
    }
    
    return notificationStringSentence;
}

@end

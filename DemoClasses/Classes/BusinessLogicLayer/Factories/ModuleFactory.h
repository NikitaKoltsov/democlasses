//
//  ModuleFactory.h
//  mix
//
//  Created by Nikita Koltsov on 3/26/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MixPlayerViewController.h"
#import "AudioTrackCutViewController.h"

@class VideoTrackCutViewController, LibraryVideoPickerViewController, TrackPreviewViewController, ProfileViewController, TrackListViewController, SearchedMixesViewController;
@class AVAsset, MXTagModel, MXMix;

@interface ModuleFactory : NSObject

+ (MixPlayerViewController *)mixPlayerViewControllerWithMix:(MXMix *)mix
                                                   delegate:(id<MixPlayerViewControllerDelegate>)delegate;

+ (SearchedMixesViewController *)searchedMixesViewControllerWithSearchTag:(NSString *)tag;
+ (SearchedMixesViewController *)searchedMixesViewControllerWithSearchTagModel:(MXTagModel *)tag;

+ (TrackListViewController *)trackListViewControllerWithMix:(MXMix *)mix featuredEdit:(BOOL)featuredEdit;
+ (TrackListViewController *)trackListViewControllerWithMix:(MXMix *)mix featuredEdit:(BOOL)featuredEdit backButtonTitle:(NSString *)backButtonTitle;
+ (TrackListViewController *)trackListViewControllerWithVideoMixModel:(VideoMixModel *)videoMixModel backButtonTitle:(NSString *)backButtonTitle;

+ (ProfileViewController *)profileViewControllerWithUser:(MXUser *)user;

+ (AudioTrackCutViewController *)audioTrackCutViewControllerWithDelegate:(id<AudioTrackCutViewControllerDelegate>)delegate
                                                                userInfo:(NSDictionary *)userInfo;

+ (TrackPreviewViewController *)trackPreviewViewControllerWithVideoMixModel:(VideoMixModel *)videoMixModel
                                                              videoFilePath:(NSString *)videoFilePath
                                                                 trackImage:(UIImage *)trackImage;

+ (LibraryVideoPickerViewController *)libraryVideoPickerViewControllerWithVideoMixModel:(VideoMixModel *)videoMixModel;

+ (VideoTrackCutViewController *)videoTrackCutViewControllerWithAsset:(AVAsset *)asset videoMixModel:(VideoMixModel *)videoMixModel;

@end

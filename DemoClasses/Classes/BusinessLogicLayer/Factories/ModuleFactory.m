//
//  ModuleFactory.m
//  mix
//
//  Created by Nikita Koltsov on 3/26/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import "ModuleFactory.h"
#import <AVFoundation/AVFoundation.h>

#import "MXConstants.h"

#import "ProfileViewController.h"
#import "TrackListViewController.h"
#import "SearchedMixesViewController.h"
#import "LibraryVideoPickerViewController.h"
#import "VideoTrackCutViewController.h"

@implementation ModuleFactory

#pragma mark - MixPlayerViewController

+ (MixPlayerViewController *)mixPlayerViewControllerWithMix:(MXMix *)mix delegate:(id<MixPlayerViewControllerDelegate>)delegate {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kStoryboardNamePlayers bundle:nil];
    MixPlayerViewController *vc = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([MixPlayerViewController class])];
    vc.delegate = delegate;
    vc.mix = mix;
    
    return vc;
}

#pragma mark - SearchedMixesViewController

+ (SearchedMixesViewController *)searchedMixesViewControllerWithSearchTag:(NSString *)tag {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kStoryboardNameSearch bundle:nil];
    SearchedMixesViewController *vc = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SearchedMixesViewController class])];
    vc.textToSearch = tag;
    
    return vc;
}

+ (SearchedMixesViewController *)searchedMixesViewControllerWithSearchTagModel:(MXTagModel *)tag {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kStoryboardNameSearch bundle:nil];
    SearchedMixesViewController *vc = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SearchedMixesViewController class])];
    vc.tagToSearch = tag;
    
    return vc;
}

#pragma mark - TrackListViewController

+ (TrackListViewController *)trackListViewControllerWithMix:(MXMix *)mix featuredEdit:(BOOL)featuredEdit {
    return [self trackListViewControllerWithMix:mix featuredEdit:featuredEdit backButtonTitle:nil];
}

+ (TrackListViewController *)trackListViewControllerWithMix:(MXMix *)mix featuredEdit:(BOOL)featuredEdit backButtonTitle:(NSString *)backButtonTitle {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kStoryboardNameMix bundle:nil];
    TrackListViewController *vc = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([TrackListViewController class])];
    vc.featuredEdit = featuredEdit;
    
    VideoMixModel *mixModel = [[VideoMixModel alloc] init];
    mixModel.mix = mix;
    mixModel.mixToTrackArray = [NSMutableArray arrayWithArray:[mix.mixToTracks allObjects]];
    vc.videoMixModel = mixModel;
    
    if (backButtonTitle) {
        vc.backButtonTitle = backButtonTitle;
    }
    
    return vc;
}

+ (TrackListViewController *)trackListViewControllerWithVideoMixModel:(VideoMixModel *)videoMixModel backButtonTitle:(NSString *)backButtonTitle {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kStoryboardNameMix bundle:nil];
    TrackListViewController *vc = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([TrackListViewController class])];
    
    vc.videoMixModel = videoMixModel;
    
    if (backButtonTitle) {
        vc.backButtonTitle = backButtonTitle;
    }
    
    return vc;
}

#pragma mark - ProfileViewController

+ (ProfileViewController *)profileViewControllerWithUser:(MXUser *)user {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kStoryboardNameProfile bundle:nil];
    ProfileViewController *vc = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass(ProfileViewController.class)];
    vc.user = user;
    
    return vc;
}

#pragma mark - AudioTrackCutViewController

+ (AudioTrackCutViewController *)audioTrackCutViewControllerWithDelegate:(id<AudioTrackCutViewControllerDelegate>)delegate
                                                                userInfo:(NSDictionary *)userInfo {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kStoryboardNamePlayers bundle:nil];
    AudioTrackCutViewController *vc = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([AudioTrackCutViewController class])];
    
    vc.audioAsset = userInfo[kAddTrackPropertyKeyAudioAsset];
    vc.trackName = userInfo[kAddTrackPropertyKeyTrackName];
    vc.artistName = userInfo[kAddTrackPropertyKeyArtistName];
    vc.artworkImage = userInfo[kAddTrackPropertyKeyArtwork];
    vc.desiredDuration = userInfo[kCutAudioTrackPropertyKeyDesiredDuration];
    vc.videoMixModel = userInfo[kCutAudioTrackPropertyKeyVideoMixModel];
    vc.delegate = delegate;
    
    return vc;
}

#pragma mark - TrackPreviewViewController

+ (TrackPreviewViewController *)trackPreviewViewControllerWithVideoMixModel:(VideoMixModel *)videoMixModel
                                                              videoFilePath:(NSString *)videoFilePath
                                                                 trackImage:(UIImage *)trackImage {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kStoryboardNamePlayers bundle:nil];
    
    TrackPreviewViewController *previewController = [storyboard instantiateViewControllerWithIdentifier:
                                                     NSStringFromClass([TrackPreviewViewController class])];
    previewController.videoMixModel = videoMixModel;
    previewController.videoFilePath = videoFilePath;
    previewController.trackImage = trackImage;
    
    return previewController;
}

#pragma mark - LibraryVideoPickerViewController

+ (LibraryVideoPickerViewController *)libraryVideoPickerViewControllerWithVideoMixModel:(VideoMixModel *)videoMixModel {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kStoryboardNameMix bundle:nil];
    LibraryVideoPickerViewController *vc = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([LibraryVideoPickerViewController class])];
    vc.videoMixModel = videoMixModel;
    
    return vc;
}

#pragma mark - VideoTrackCutViewController

+ (VideoTrackCutViewController *)videoTrackCutViewControllerWithAsset:(AVAsset *)asset videoMixModel:(VideoMixModel *)videoMixModel {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kStoryboardNamePlayers bundle:nil];
    VideoTrackCutViewController *vc = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([VideoTrackCutViewController class])];
    vc.videoMixModel = videoMixModel;
    vc.asset = asset;
    
    return vc;
}

@end

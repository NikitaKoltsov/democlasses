//
//  MXLibraryVideoService.m
//  mix
//
//  Created by Nikita Koltsov on 4/9/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import "MXLibraryVideoService.h"

#import "MXConstants.h"

@interface MXLibraryVideoService ()

@property (strong, nonatomic) AVAssetImageGenerator *imageGenerator;

@end

@implementation MXLibraryVideoService

- (void)loadAssetsWithCompletion:(void (^)(BOOL, PHFetchResult<PHAsset *> *))completion {
    __weak __typeof(self) weakSelf = self;
    
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        switch (status) {
            case PHAuthorizationStatusDenied:
            case PHAuthorizationStatusNotDetermined:
            case PHAuthorizationStatusRestricted:
                completion(NO, nil);
                break;
            case PHAuthorizationStatusAuthorized: {
                dispatch_async(dispatch_get_main_queue(), ^{
                    PHFetchOptions *options = [PHFetchOptions new];
                    options.predicate = [NSPredicate predicateWithFormat:@"duration > %@", @(kMinRecordingTime)];
                    
                    PHFetchResult<PHAsset *> *fetchResult = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeVideo options:options];
                    weakSelf.fetchResult = fetchResult;
                    completion(YES, fetchResult);
                });
            }
                break;
        }
    }];
}

- (void)loadImageForAsset:(PHAsset *)asset withCompletion:(void (^)(UIImage *))completion {
    PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
    options.resizeMode = PHImageRequestOptionsResizeModeExact;
    options.networkAccessAllowed = YES;
    options.synchronous = YES;
    options.deliveryMode = PHVideoRequestOptionsDeliveryModeHighQualityFormat;
    
    NSInteger retinaMultiplier = [UIScreen mainScreen].scale;
    CGSize retinaSquare = CGSizeMake([UIScreen mainScreen].bounds.size.width * retinaMultiplier,
                                     [UIScreen mainScreen].bounds.size.height * retinaMultiplier);
    [[PHImageManager defaultManager] requestImageForAsset:asset
                                               targetSize:retinaSquare
                                              contentMode:PHImageContentModeAspectFill
                                                  options:options
                                            resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
                                                completion(result);
                                            }];
}

- (void)loadAVAssetForAsset:(PHAsset *)asset withCompletion:(void (^)(AVAsset *))completion {
    PHVideoRequestOptions *options = [PHVideoRequestOptions new];
    options.deliveryMode = PHVideoRequestOptionsDeliveryModeHighQualityFormat;
    
    [[PHImageManager defaultManager] requestAVAssetForVideo:asset
                                                    options:options
                                              resultHandler:^(AVAsset * _Nullable asset, AVAudioMix * _Nullable audioMix, NSDictionary * _Nullable info) {
                                                  completion(asset);
                                              }];
}

- (UIImage *)imageForAVAsset:(AVAsset *)asset {
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generator.appliesPreferredTrackTransform = YES;
    
    CMTime time = CMTimeMakeWithSeconds(0.0, 600);
    CMTime actualTime;
    CGImageRef imageRef = [generator copyCGImageAtTime:time actualTime:&actualTime error:nil];
    if (imageRef != NULL) {
        return [[UIImage alloc] initWithCGImage:imageRef];
    } else {
        return nil;
    }
}

- (void)imagesArrayForAVAsset:(AVAsset *)asset step:(float)step withCompletion:(void (^)(NSArray *))completion {
    self.imageGenerator = [AVAssetImageGenerator assetImageGeneratorWithAsset:asset];
    self.imageGenerator.appliesPreferredTrackTransform = YES;
    
    NSMutableArray *times = [NSMutableArray array];
    Float64 durationInSeconds = CMTimeGetSeconds(asset.duration);
    
    float currentSecond = 0.0;
    while (currentSecond < durationInSeconds) {
        [times addObject:[NSValue valueWithCMTime:CMTimeMakeWithSeconds(currentSecond, durationInSeconds)]];
        currentSecond += step;
    }
    // If we have more than half of step extra time then we want to get one extra picture
    if (step / 2 < durationInSeconds - currentSecond) {
        [times addObject:[NSValue valueWithCMTime:CMTimeMakeWithSeconds(durationInSeconds - 1.0, durationInSeconds)]];
    }
    
    NSMutableArray *images = [NSMutableArray array];
    [self.imageGenerator generateCGImagesAsynchronouslyForTimes:times
                                              completionHandler:^(CMTime requestedTime,
                                                                  CGImageRef  _Nullable image,
                                                                  CMTime actualTime,
                                                                  AVAssetImageGeneratorResult result,
                                                                  NSError * _Nullable error) {
                                                  NSString *requestedTimeString = @(CMTimeGetSeconds(requestedTime)).stringValue;
                                                  NSString *actualTimeString = @(CMTimeGetSeconds(actualTime)).stringValue;
                                                  NSLog(@"Requested: %@; actual %@", requestedTimeString, actualTimeString);
                                                  
                                                  if (image) {
                                                      [images addObject:[UIImage imageWithCGImage:image]];
                                                  }
                                                  if (images.count == times.count) {
                                                      completion(images);
                                                  }
                                              }];
}

@end

//
//  MXLibraryVideoService.h
//  mix
//
//  Created by Nikita Koltsov on 4/9/18.
//  Copyright © 2018 Xylo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>
#import <AVFoundation/AVFoundation.h>

@interface MXLibraryVideoService : NSObject

@property (strong, nonatomic) PHFetchResult <PHAsset *> *fetchResult;

- (void)loadAssetsWithCompletion:(void (^)(BOOL, PHFetchResult<PHAsset *> *))completion;
- (void)loadImageForAsset:(PHAsset *)asset withCompletion:(void (^)(UIImage *))completion;
- (void)loadAVAssetForAsset:(PHAsset *)asset withCompletion:(void (^)(AVAsset *))completion;

- (UIImage *)imageForAVAsset:(AVAsset *)asset;
- (void)imagesArrayForAVAsset:(AVAsset *)asset step:(float)step withCompletion:(void (^)(NSArray *))completion;

@end

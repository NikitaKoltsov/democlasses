//
//  main.m
//  DemoClasses
//
//  Created by Nikita Koltsov on 4/10/18.
//  Copyright © 2018 n.koltsov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
